namespace Todoer3.Models;

public class Todo {
  public int Id { get; set; }

  public string? Title { get; set; }

  public bool Completed { get; set; }

  public string? IdentityUserId { get; set; }
  public IdentityUser? IdentityUser { get; set; }
}